
import Footer from './Component/Footer';
import MainOfRes from './Component/RestaurantData/MainComponent';
import Restaurant1Data1 from './Component/RestaurantData/Restaurant1Part1';
import Restaurant1Data2 from './Component/RestaurantData/Restaurant1Part2';
import Restaurant1Data3 from './Component/RestaurantData/Restaurant1Part3';
import Restaurant1Data4 from './Component/RestaurantData/Restaurant1Part4';
import Restaurant1Data5 from './Component/RestaurantData/Restaurant1Part5';
import Player1Data1 from './Component/PlayerData/Player1Data1';
import Player1Data2 from './Component/PlayerData/Player1Data2';
import Player1Data3 from './Component/PlayerData/Player1Data3';
import Player1Data4 from './Component/PlayerData/Player1Data4';
import Player1Data5 from './Component/PlayerData/Player1Data5';

import MoreAboutRestaurantPlayer6 from './Component/PlayerData/MoreAboutRestaurantPlayer6';
import MoreAboutRestaurantPlayer7 from './Component/PlayerData/MoreAboutRestaurantPlayer7';
import MoreAboutRestaurantPlayer8 from './Component/PlayerData/MoreAboutRestaurantPlayer8';
import MoreAboutRestaurantPlayer9 from './Component/PlayerData/MoreAboutRestaurantPlayer9';
import MoreAboutRestaurantPlayer10 from './Component/PlayerData/MoreAboutRestaurantPlayer10';

import PlayerRag from './Component/PlayerRagestationPage';
import RestaurantRag from './Component/RestaurantRagestationPage';
import MainPlayer from './Component/PlayerData/MainComponentOfPlayer';
import 'bootstrap/dist/css/bootstrap.min.css';
// import './App.css'
import { BrowserRouter, Routes, Route } from "react-router-dom";


import HomePage from './Component/HomePage';
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<MainOfRes />} />
          
          <Route path="/PlayerRag" element={<PlayerRag />} >   </Route>
          <Route path="/RestaurantRag" element={<RestaurantRag />} />
          <Route path="/MainOfRes" element={<MainOfRes />} />
          <Route path="/MainPlayer" element={<MainPlayer />} />
          <Route path="/Restaurant1Data1" element={<Restaurant1Data1 />} />
          <Route path="/Restaurant1Data2" element={<Restaurant1Data2 />} />
          <Route path="/Restaurant1Data3" element={<Restaurant1Data3 />} />
          <Route path="/Restaurant1Data4" element={<Restaurant1Data4 />} /> 
         
          <Route path="/Restaurant1Data5" element={<Restaurant1Data5 />} />
          <Route path="/Player1Data1" element={<Player1Data1 />} />
          <Route path="/Player1Data2" element={<Player1Data2 />} />
          <Route path="/Player1Data3" element={<Player1Data3 />} />
          <Route path="/Player1Data4" element={<Player1Data4 />} />
          <Route path="/Player1Data5" element={<Player1Data5 />} />
          <Route path="/MoreAboutRestaurantPlayer6" element={<MoreAboutRestaurantPlayer6 />} /> 
          <Route path="/MoreAboutRestaurantPlayer7" element={<MoreAboutRestaurantPlayer7 />} />
          <Route path="/MoreAboutRestaurantPlayer8" element={<MoreAboutRestaurantPlayer8 />} />
          <Route path="/MoreAboutRestaurantPlayer9" element={<MoreAboutRestaurantPlayer9 />} />
          <Route path="/MoreAboutRestaurantPlayer10" element={<MoreAboutRestaurantPlayer10 />} />
      
        </Routes>
      </BrowserRouter>

      <Footer />
    </div>
  );
}

export default App;
