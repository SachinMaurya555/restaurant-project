import Table from 'react-bootstrap/Table';
import React, { useEffect, useState } from 'react'
// import './TableCss.css'
import './MainComponentOfRest.css'
import logo from "../../IMG/Restorent.jpg";
import { Link } from 'react-router-dom';
import HomePage from '../HomePage';


function Restaurant1Data5() {

    const [users, setUser] = useState([])
    useEffect(() => {
        fetch(`/restaurant/getRestaurant/POTLI RESTAURANT`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users)


    return (
        <>
            <HomePage />
            <div className='container'>
                <Link to='/Restaurant1Data5' style={{ textDecoration: 'none' }}>
                    <h2 style={{ color: 'Blue', margin: '20px' }} >{users.name} </h2>
                </Link>
                <div>

                    <div className='AllRowStyle'>
                        <Table striped bordered hover >
                            <thead >
                                <tr className='TableStyle' >

                                    <th>Name</th>
                                    <th>Contact Number</th>
                                    <th>Street Address 1</th>
                                    <th>Street Address 2 </th>
                                    <th>Street NO </th>
                                    <th>Country </th>
                                    <th>State </th>
                                    <th>Postal </th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr >


                                    <td>{users.name}</td>
                                    <td>{users.contact_number}</td>
                                    <td>{users.r_address_line1}</td>
                                    <td>{users.r_address_line2}</td>
                                    <td>{users.r_street_no}</td>
                                    <td>{users.r_country}</td>
                                    <td>{users.r_state}</td>
                                    <td>{users.r_postal}</td>

                                </tr>

                            </tbody>
                        </Table>
                        <Table striped bordered hover>
                            <thead>
                                <tr className='TableStyle' >

                                    <th>MondayOpeningTime </th>
                                    <th>MondayClosingTime </th>
                                    <th>TuesdayOpeningTime </th>
                                    <th>TuesdayClosingTime </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr >

                                    <td>{users.mondayOpeningTime}</td>
                                    <td>{users.mondayClosingTime}</td>
                                    <td>{users.tuesdayOpeningTime}</td>
                                    <td>{users.tuesdayClosingTime}</td>

                                </tr>

                            </tbody>
                        </Table>
                        <Table striped bordered hover>
                            <thead>
                                <tr className='TableStyle' >

                                    <th>WednesdayOpeningTime </th>

                                    <th>WednesdayClosingTime </th>
                                    <th> ThursdayOpeningTime </th>
                                    <th>ThursdayClosingTime </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr >

                                    <td>{users.wednesdayOpeningTime}</td>
                                    <td>{users.wednesdayClosingTime}</td>
                                    <td>{users.thursdayOpeningTime}</td>
                                    <td>{users.thursdayClosingTime}</td>

                                </tr>

                            </tbody>
                        </Table>
                        <Table striped bordered hover>
                            <thead>
                                <tr className='TableStyle' >
                                    <th>FridayOpeningTime </th>
                                    <th>FridayClosingTime </th>
                                    <th>SaturdayOpeningTime </th>
                                    <th>SaturdayClosingTime </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr >

                                    <td>{users.fridayOpeningTime}</td>
                                    <td>{users.fridayClosingTime}</td>
                                    <td>{users.saturdayOpeningTime}</td>
                                    <td>{users.saturdayClosingTime}</td>

                                </tr>

                            </tbody>
                        </Table>
                        <Table striped bordered hover>
                            <thead>
                                <tr className='TableStyle' >

                                    <th>SundayOpeningTime </th>
                                    <th>SundayClosingTime </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr >


                                    <td>{users.sundayOpeningTime}</td>
                                    <td>{users.sundayClosingTime}</td>

                                </tr>

                            </tbody>
                        </Table>
                    </div>
                </div>

            </div>
        </>
    );
}

export default Restaurant1Data5;