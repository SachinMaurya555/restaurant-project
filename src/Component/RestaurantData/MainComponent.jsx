import React, { useState } from "react";
import Restaurant1Data from "./Restaurant1";
import HomePage from "../HomePage";
import './MainComponentOfRest.css'
import logo from "../../IMG/Restorent.jpg";


const Main = () => {



  return (
    <>
      <HomePage />
      <main>
        <section className="menu section">
          <div className="title">
            <img src={logo} alt="logo" className="logo" />
            <h2 style={{ color: 'rgb(0, 12, 255)' }}>Restaurant
              List</h2>
            <div id="underline2" className="underline"></div>
          </div>
          <Restaurant1Data />


        </section>
      </main>
    </>
  );
};

export default Main;