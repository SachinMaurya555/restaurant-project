import Table from 'react-bootstrap/Table';
import React, { useEffect, useState } from 'react'
// import './TableCss.css'
import './MainComponentOfRest.css'
import './AllRestaurantPart.css'
import logo from "../../IMG/Restorent.jpg";
import { Link } from 'react-router-dom';
import Restaurant1Data1 from './Restaurant1Part1';



function Restaurant1Data() {

    const [users, setUser] = useState([])
    const [users2, setUser2] = useState([])
    const [users3, setUser3] = useState([])
    const [users4, setUser4] = useState([])
    const [users5, setUser5] = useState([])

    useEffect(() => {
        fetch(`/restaurant/getRestaurant/WoW MoMo`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users)

    useEffect(() => {
        fetch(`/restaurant/getRestaurant/21 Gun Salute`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser2(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users2)

    useEffect(() => {
        fetch(`/restaurant/getRestaurant/Cafe Amaretto`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser3(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users3)

    useEffect(() => {
        fetch(`/restaurant/getRestaurant/Seasonal Tastes`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser4(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users4)

    useEffect(() => {
        fetch(`/restaurant/getRestaurant/POTLI RESTAURANT`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser5(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users5)


    return (
        <div>
            <div className='AddRestorent'>
                <Link to='/RestaurantRag' style={{ textDecoration: 'none' }}>
                    <h3 style={{ color: 'red', display: 'flex', alignContent: 'flex-end' }} > Add Restaurant</h3>
                </Link>
            </div>
            <div>

                <div className='AllRowStyle'>
                    <Table striped bordered hover >
                        <thead >
                            <tr className='TableStyle' >

                                <th>Name</th>
                                <th>Contact Number</th>
                                <th>Street Address </th>



                            </tr>
                        </thead>
                        <tbody>
                            <tr >


                                <td>{users.name}</td>
                                <td>{users.contact_number}</td>
                                <td>{users.r_address_line1}</td>
                                <td> <Link to='/Restaurant1Data1' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View More</td>
                                </Link>
                                </td>
                            </tr>

                        </tbody>
                        <tbody>
                            <tr >


                                <td>{users2.name}</td>
                                <td>{users2.contact_number}</td>
                                <td>{users2.r_address_line1}</td>
                                <td> <Link to='/Restaurant1Data2' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View More</td>
                                </Link>
                                </td>
                            </tr>

                        </tbody>
                        <tbody>
                            <tr >


                                <td>{users3.name}</td>
                                <td>{users3.contact_number}</td>
                                <td>{users3.r_address_line1}</td>
                                <td> <Link to='/Restaurant1Data3' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View More</td>
                                </Link>
                                </td>
                            </tr>

                        </tbody>
                        <tbody>
                            <tr >


                                <td>{users4.name}</td>
                                <td>{users4.contact_number}</td>
                                <td>{users4.r_address_line1}</td>
                                <td> <Link to='/Restaurant1Data4' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View More</td>
                                </Link>
                                </td>
                            </tr>

                        </tbody>
                        <tbody>
                            <tr >


                                <td>{users5.name}</td>
                                <td>{users5.contact_number}</td>
                                <td>{users5.r_address_line1}</td>
                                <td> <Link to='/Restaurant1Data5' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View More</td>
                                </Link>
                                </td>
                            </tr>

                        </tbody>
                    </Table>




                </div>
            </div>

        </div>
    );
}

export default Restaurant1Data;