import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import './Navbar.css'
import Navbar from 'react-bootstrap/Navbar';
import logo from '../IMG/logo.png';   
import { Link } from 'react-router-dom';

function NavbarComponent() {
  return (  

    // bg="secondary"     
    <Navbar id='BgBackColor'    expand="lg"> 
    <img src={logo} style={{marginLeft: '25px'}} width="60px" height="55px" />
      <Container fluid>
        
        <Navbar.Brand  style={{color:'white' ,fontSize: "27px"}} href="/">Restaurant</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
        <Nav className="justify-content-end flex-grow-1 pe-3"
            style={{ maxHeight: '100px', }} 
            navbarScroll                       

          >
            <Nav.Link style={{color:'white' ,fontSize: "22px"}} href="/">Home</Nav.Link>
            <Nav.Link style={{color:'white' ,fontSize: "22px"}} href="/MainPlayer">Player</Nav.Link>
            <Nav.Link style={{color:'white' ,fontSize: "22px"}} href="/MainOfRes">Restaurant</Nav.Link>
           
          </Nav>
          {/* <Form className="d-flex">
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search" 
            />
            <Button variant="success">Search</Button>
          </Form> */}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavbarComponent;