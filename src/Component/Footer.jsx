import React from "react";
import { Link } from 'react-router-dom';
import Nav from 'react-bootstrap/Nav';
import MobileIMG from './images/mobile.png';
import google_play from './images/google_play.png';
import AboutIMG from './images/about-img.jpg';

import app_store from './images/google_play.png'; 
import './css/responsive.css'     // IMG_n2
import './css/style.css'
import './Footer.css'
const Footer = () => {
    return (

        <div>


            {/* <!-- app section --> */}

            <section className="app_section">
                <div className="container">
                    <div className="col-md-9 mx-auto">
                        <div className="row">
                            <div className="col-md-7 col-lg-8">
                                <div className="detail-box">
                                    <h2 style={{ color: 'currentColor' }}>
                                        <span> Get the</span> <br />
                                        Restaurant App
                                    </h2>
                                    <p style={{ color: 'white' }}>
                                        Long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The poin
                                    </p>
                                    <div className="app_btn_box">
                                        <a href="" className="mr-1">
                                            <img src={google_play} className="box-img" alt="" />
                                        </a>
                                        <a href="">
                                            <img src={app_store} className="box-img" alt="" />
                                        </a>
                                    </div>
                                    <a href="" className="download_btn">
                                        Download Now
                                    </a>
                                </div>
                            </div>
                            <div className="col-md-5 col-lg-4">
                                <div className="img-box">
                                    <img src={MobileIMG} className="box-img" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            {/* <!-- end app section --> */}

            {/* <!-- about section --> */}

            <section className="about_section layout_padding">
                <div className="container">
                    <div className="col-md-11 col-lg-10 mx-auto">
                        <div id="heading_container1" className="heading_container  heading_center">
                            <h2>
                                About Us
                            </h2>
                        </div>
                        <div className="box">
                            <div className="col-md-7 mx-auto">
                                <div className="img-box">
                                    <img src={AboutIMG} className="box-img" alt="" />
                                </div>
                            </div>
                            <div className="detail-box">
                                <p>
                                    A restaurant is a business that prepares and serves food and drinks to customers. Meals are generally served and eaten on the premises, but many restaurants also offer take-out and food delivery services. Restaurants vary greatly in appearance and offerings.
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {/* <!-- end about section --> */}

            {/* <!-- end news section --> */}

            {/* <!-- client section --> */}

            <section className="client_section layout_padding">
                <div className="container">
                    <div className="col-md-11 col-lg-10 mx-auto">
                        <div id="heading_container2" className="heading_container heading_center">
                            <h2>
                                Testimonial
                            </h2>
                        </div>
                        <div id="customCarousel1" className="carousel slide" data-ride="carousel">
                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <div className="detail-box">
                                        <h4>
                                            Virginia
                                        </h4>
                                        <p>
                                            The restaurant may not boast of a huge size, but it sure does exhibit an inviting ambiance and a lovely décor. If you are a book lover, you will love spending time at this restaurant. Of course, for non-book lovers, the incentive to visit this place is always the delectable food served here. You can get to taste dishes from Mediterranean and Continental cuisines along with café delights.
                                        </p>
                                        <i className="fa fa-quote-left" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div className="detail-box">
                                        <h4>
                                            Virginia
                                        </h4>
                                        <p>
                                            Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and
                                        </p>
                                        <i className="fa fa-quote-left" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div className="detail-box">
                                        <h4>
                                            Virginia
                                        </h4>
                                        <p>
                                            The restaurant may not boast of a huge size, but it sure does exhibit an inviting ambiance and a lovely décor. If you are a book lover, you will love spending time at this restaurant. Of course, for non-book lovers, the incentive to visit this place is always the delectable food served here. You can get to taste dishes from Mediterranean and Continental cuisines along with café delights.
                                        </p>
                                        <i className="fa fa-quote-left" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                            <a className="carousel-control-prev d-none" href="#customCarousel1" role="button" data-slide="prev">
                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span className="sr-only">Previous</span>
                            </a>
                            <a className="carousel-control-next" href="#customCarousel1" role="button" data-slide="next">
                                <i className="fa fa-arrow-right" aria-hidden="true"></i>
                                <span className="sr-only"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </section>

            {/* <!-- end client section --> */}

            <div className="footer_container">
                {/* <!-- info section --> */}
                <section className="info_section ">
                    <div className="container">
                        <div className="contact_box">
                            <a href="">
                                <i className="fa fa-map-marker" aria-hidden="true"></i>
                            </a>
                            <a href="">
                                <i className="fa fa-phone" aria-hidden="true"></i>
                            </a>
                            <a href="">
                                <i className="fa fa-envelope" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div className="info_links">
                            <ul>
                                <li>
                                    <Nav.Link style={{ color: 'white', fontSize: "17px" }} href="/MainOfRes">Home</Nav.Link>
                                </li>
                                <li>
                                    <Nav.Link style={{ color: 'white', fontSize: "17px" }} href="/MainPlayer">Player List</Nav.Link>

                                </li>
                                <li>
                                    <Nav.Link style={{ color: 'white', fontSize: "17px" }} href="/RestaurantRag">Restaurant Registration</Nav.Link>
                                </li>
                                <li>
                                    <Nav.Link style={{ color: 'white', fontSize: "17px" }} href="/PlayerRag">Player Registration</Nav.Link>
                                </li>
                            </ul>
                        </div>
                        <div className="social_box">
                            <a href="">
                                <i className="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                            <a href="">
                                <i className="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                            <a href="">
                                <i className="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </section>
                {/* <!-- end info_section --> */}


                {/* <!-- footer section --> */}
                <footer className="footer_section">
                    <div className="container">
                        <p>
                            &copy; <span id="displayYear"></span> All Rights Reserved By
                            <a> Pro Restaurant</a>

                        </p>
                    </div>
                </footer>
                {/* <!-- footer section --> */}
            </div>
        </div>
    )
};

export default Footer;