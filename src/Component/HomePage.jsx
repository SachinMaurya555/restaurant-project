import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import './Navbar.css'
import Navbar from 'react-bootstrap/Navbar';
import logo from '../IMG/logo.png';
import './HomePage.css'
import InputForm from './InputPage';
import { Link } from "react-router-dom";

const HomePage = () => {
    return (
        // bg="secondary"     
        <Navbar id='BgBackColor' expand="lg">
            <img src={logo} style={{ marginLeft: '25px' }} width="60px" height="55px" />
            <Container fluid>

                <Link style={{ color: 'white' , textDecoration: 'none', fontSize: "27px" }}  to="/MainOfRes">Restaurant</Link>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Nav className="justify-content-end flex-grow-1 pe-3"
                        style={{ maxHeight: '100px', }}
                        navbarScroll

                    >
                        <Link style={{ color: 'white', textDecoration: 'none', margin: "0px 5px", fontSize: "22px" }} to="/MainOfRes">Home</Link>
                        <Link style={{ color: 'white', textDecoration: 'none', margin: "0px 5px", fontSize: "22px" }} to="/MainPlayer">Player</Link>
                        <Link style={{ color: 'white', textDecoration: 'none', margin: "0px 5px", fontSize: "22px" }} to="/MainOfRes">Restaurant</Link>

                    </Nav>

                </Navbar.Collapse>
            </Container>
        </Navbar>

    )

}

export default HomePage;