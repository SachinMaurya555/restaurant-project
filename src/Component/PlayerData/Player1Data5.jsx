import Table from 'react-bootstrap/Table';
import React, { useEffect, useState } from 'react';
import './MainComponentOfPlayer.css'
import HomePage from '../HomePage';
import { Link } from 'react-router-dom';


function Player1Data5() {

    const [users, setUser] = useState([])
    useEffect(() => {
        fetch(`/player/getPlayer/Ankit/Kumar`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users)


    return (
        <>
            <HomePage />

            <div className='container'>
                <Link to='/Player1Data5' style={{ textDecoration: 'none' }}>
                    <h2 style={{ color: 'Blue', margin: '20px' }} > {users.firstName} {users.lastName}</h2>
                </Link>
                <div>

                    <div className='AllRowStyle'>
                        <Table striped bordered hover >
                            <thead >
                                <tr className='TableStyle' >

                                    <th>Name</th>
                                    <th>Contact Number</th>
                                    <th>Email </th>
                                    <th>Date Of Birth</th>
                                    <th>Age </th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr >


                                    <td>{users.firstName} {users.lastName}</td>
                                    <td>{users.phone_number}</td>
                                    <td>{users.email}</td>
                                    <td>{users.dob}</td>
                                    <td>{users.age}</td>


                                </tr>

                            </tbody>
                        </Table>
                        <Table striped bordered hover>
                            <thead>
                                <tr className='TableStyle' >
                                    <th>Street Address 1</th>
                                    <th>Street Address 2 </th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr >

                                    <td>{users.p_address_line1}</td>
                                    <td>{users.p_address_line2}</td>


                                </tr>

                            </tbody>
                        </Table>
                        <Table striped bordered hover>
                            <thead>
                                <tr className='TableStyle' >
                                    <th>Country </th>
                                    <th>State </th>
                                    <th> City</th>
                                    <th>Street NO </th>
                                    <th>Postal </th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr >

                                    <td>{users.p_country}</td>
                                    <td>{users.p_state}</td>
                                    <td>{users.p_city}</td>

                                    <td>{users.p_street_no}</td>
                                    <td>{users.p_postal}</td>



                                </tr>

                            </tbody>
                        </Table>
                        <Table striped bordered hover>
                            <thead>
                                <tr className='TableStyle' >
                                    <th>Alternate Address-1 </th>
                                    <th>Alternate Address-2 </th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr >

                                    <td>{users.a_address_line1}</td>
                                    <td>{users.a_address_line2}</td>


                                </tr>

                            </tbody>
                        </Table>
                        <Table striped bordered hover>
                            <thead>
                                <tr className='TableStyle' >
                                    <th>Country </th>
                                    <th>State </th>
                                    <th> City</th>
                                    <th>Street NO </th>
                                    <th>Postal </th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr >

                                    <td>{users.a_country}</td>
                                    <td>{users.a_state}</td>
                                    <td>{users.a_city}</td>

                                    <td>{users.a_street_no}</td>
                                    <td>{users.a_postal}</td>



                                </tr>

                            </tbody>
                        </Table>
                        <Table striped bordered hover>
                            <thead>
                                <tr className='TableStyle' >

                                    <th>Office Address-1 </th>
                                    <th>Office Address-2 </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr >

                                    <td>{users.o_address_line1}</td>
                                    <td>{users.o_address_line2}</td>


                                </tr>

                            </tbody>
                        </Table>
                        <Table striped bordered hover>
                            <thead>
                                <tr className='TableStyle' >
                                    <th>Country </th>
                                    <th>State </th>
                                    <th> City</th>
                                    <th> Unit NO </th>
                                    <th>Postal </th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr >

                                    <td>{users.o_country}</td>
                                    <td>{users.o_state}</td>
                                    <td>{users.o_city}</td>

                                    <td>{users.o_unit_no}</td>
                                    <td>{users.o_postal}</td>



                                </tr>

                            </tbody>
                        </Table>

                        <Table striped bordered hover>
                            <thead>
                                <tr className='TableStyle' >
                                    <th>Driver’s License </th>
                                    <th>Passport </th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr >

                                    <td>{users.driving_license}</td>
                                    <td>{users.passport}</td>


                                </tr>

                            </tbody>
                        </Table>

                    </div>
                </div>

            </div>
        </>
    );
}

export default Player1Data5;