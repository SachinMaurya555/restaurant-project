import Table from 'react-bootstrap/Table';
import React, { useEffect, useState } from 'react'
// import './TableCss.css' 
import './AllMainPlayerCSS.css'
import './MainComponentOfPlayer.css'
import MoreAboutRestaurantPlayer9 from './MoreAboutRestaurantPlayer9';

import logo from "../../IMG/Restorent.jpg";
import { Link } from 'react-router-dom';




function PlayerMainData() {

    const [usersFavR1, setUserFavR1] = useState([])
    const [usersFavR2, setUserFavR2] = useState([])
    const [usersFavR3, setUserFavR3] = useState([])
    const [usersFavR4, setUserFavR4] = useState([])
    const [usersFavR5, setUserFavR5] = useState([])

    const [users3, setUser3] = useState([])
    const [users4, setUser4] = useState([])
    const [users5, setUser5] = useState([])
    const [users2, setUser2] = useState([])
    const [users, setUser] = useState([])

    useEffect(() => {
        fetch(`/player/getPlayer/Sanjay/Singh`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users)

    useEffect(() => {
        fetch(`/player/getPlayer/Shyam /Kumar`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser2(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users2)

    useEffect(() => {
        fetch(`/player/getPlayer/Sandeep/Yadav`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser3(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users3)

    useEffect(() => {
        fetch(`/player/getPlayer/Rahul/Kumar`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser4(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users4)

    useEffect(() => {
        fetch(`/player/getPlayer/Ankit/Kumar`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser5(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users5)

    useEffect(() => {
        fetch(`/favourite/player/Sanjay/get`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUserFavR1(resp)
            })
        })
    }, [])
    console.warn(usersFavR1)

    useEffect(() => {
        fetch(`/favourite/player/Shyam /get`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUserFavR2(resp)
            })
        })
    }, [])
    console.warn(usersFavR2)


    useEffect(() => {
        fetch(`/favourite/player/Sandeep/get`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUserFavR3(resp)
            })
        })
    }, [])
    console.warn(usersFavR3)


    useEffect(() => {
        fetch(`/favourite/player/Rahul/get`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUserFavR4(resp)
            })
        })
    }, [])
    console.warn(usersFavR4)

    useEffect(() => {
        fetch(`/favourite/player/Ankit/get`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUserFavR5(resp)
            })
        })
    }, [])
    console.warn(usersFavR5)



    return (
        <div>
            <div className='PlayerRagPage'>
                <Link to='/PlayerRag' style={{ textDecoration: 'none' }}>
                    <h3 style={{ color: 'red', display: 'flex', alignContent: 'flex-end' }} > Add Player</h3>
                </Link>
            </div>
            <div>

                <div className='AllRowStyle'>
                    <Table striped bordered hover >
                        <thead >
                            <tr className='TableStyle' >

                                <th className='Smallline'>S. No</th>
                                <th >Name</th>
                                <th >Favorite Restaurant Name</th>
                                <th >All Restaurants Details</th>




                            </tr>
                        </thead>
                        <tbody>
                            <tr >

                                <td >1</td>
                                <td> <Link to='/Player1Data1' style={{ textDecoration: 'none' }}>
                                    <td>{users.firstName} {users.lastName}</td>
                                </Link>
                                </td>
                                <td >
                                    {usersFavR1.map((value) => (
                                        <div className="user">{value.restaurant.name}</div>
                                    ))}
                                </td>
                                <td>
                                    <Link to='/MoreAboutRestaurantPlayer6' style={{ textDecoration: 'none' }}>
                                        <td> About Restaurant</td>
                                    </Link>
                                </td>


                                <td> <Link to='/Player1Data1' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View All Details </td>
                                </Link>
                                </td>
                            </tr>

                        </tbody>
                        <tbody>
                            <tr >

                                <td >2</td>
                                <td> <Link to='/Player1Data2' style={{ textDecoration: 'none' }}>
                                    <td>{users2.firstName} {users2.lastName}</td>
                                </Link>
                                </td>
                                <td >{usersFavR2.map((value) => (
                                    <div className="user">{value.restaurant.name}</div>
                                ))} </td>
                                <td>
                                    <Link to='/MoreAboutRestaurantPlayer9' style={{ textDecoration: 'none' }}>
                                        <td> About Restaurant</td>
                                    </Link>
                                </td>


                                <td> <Link to='/Player1Data2' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View All Details </td>
                                </Link>
                                </td>
                            </tr>

                        </tbody>
                        <tbody>
                            <tr >

                                <td >3</td>
                                <td> <Link to='/Player1Data3' style={{ textDecoration: 'none' }}>
                                    <td>{users3.firstName} {users3.lastName}</td>
                                </Link>
                                </td>
                                <td >{usersFavR3.map((value) => (
                                    <div className="user">{value.restaurant.name}</div>
                                ))} </td>
                                <td>
                                    <Link to='/MoreAboutRestaurantPlayer8' style={{ textDecoration: 'none' }}>
                                        <td> About Restaurant</td>
                                    </Link>
                                </td>


                                <td> <Link to='/Player1Data3' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View All Details </td>
                                </Link>
                                </td>
                            </tr>

                        </tbody>
                        <tbody>
                            <tr >

                                <td >4</td>
                                <td> <Link to='/Player1Data4' style={{ textDecoration: 'none' }}>
                                    <td>{users4.firstName} {users4.lastName}</td>
                                </Link>
                                </td>
                                <td >{usersFavR4.map((value) => (
                                    <div className="user">{value.restaurant.name}</div>
                                ))} </td>
                                <td>
                                    <Link to='/MoreAboutRestaurantPlayer7' style={{ textDecoration: 'none' }}>
                                        <td> About Restaurant</td>
                                    </Link>
                                </td>


                                <td> <Link to='/Player1Data4' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View All Details </td>
                                </Link>
                                </td>
                            </tr>

                        </tbody>
                        <tbody>
                            <tr >

                                <td >5</td>
                                <td> <Link to='/Player1Data5' style={{ textDecoration: 'none' }}>
                                    <td>{users5.firstName} {users5.lastName}</td>
                                </Link>

                                </td>
                                <td >{usersFavR5.map((value) => (
                                    <div className="user">{value.restaurant.name}</div>
                                ))} </td>
                                <td>
                                    <Link to='/MoreAboutRestaurantPlayer10' style={{ textDecoration: 'none' }}>
                                        <td> About Restaurant</td>
                                    </Link>
                                </td>


                                <td> <Link to='/Player1Data5' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View All Details </td>
                                </Link>
                                </td>
                            </tr>

                        </tbody>
                    </Table>




                </div>




            </div>

        </div>
    );
}

export default PlayerMainData;