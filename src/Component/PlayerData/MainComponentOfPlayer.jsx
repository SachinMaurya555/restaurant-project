import React, { useState } from "react";
import PlayerMainData from "./Player1";
import './MainComponentOfPlayer.css'
import logo from "../../IMG/Player.png";
import HomePage from "../HomePage";

const Main = () => {

  return (
    <>
      <HomePage />
      <main>
        <section className="menu section">
          <div className="title">
            <img src={logo} alt="logo" className="logo1" />
            <h2 style={{ color: 'rgb(0, 12, 255)' }}>Player
              List</h2>
            <div className="underline"></div>
          </div>
          <PlayerMainData />

        </section>
      </main>
    </>
  );
};

export default Main;