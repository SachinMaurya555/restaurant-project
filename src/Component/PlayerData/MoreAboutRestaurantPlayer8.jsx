import HomePage from '../HomePage';
import './MoreAboutRestaurantCSS.css'
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import React, { useEffect, useState } from 'react'
import '../RestaurantData/MainComponentOfRest.css'
import '../RestaurantData/AllRestaurantPart.css'
import logo from "../../IMG/Restorent.jpg";
import { Link } from 'react-router-dom';


const MoreAboutRestaurantPlayer8 = () => {




    const [users, setUser] = useState([])
    const [users2, setUser2] = useState([])
    const [users3, setUser3] = useState([])
    const [users4, setUser4] = useState([])
    const [users5, setUser5] = useState([])

    const [userFav, setUserFav] = useState({
        favouriteRestaurant: '',


    })

    useEffect(() => {
        fetch(`/restaurant/getRestaurant/WoW MoMo`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users)

    useEffect(() => {
        fetch(`/restaurant/getRestaurant/21 Gun Salute`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser2(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users2)

    useEffect(() => {
        fetch(`/restaurant/getRestaurant/Cafe Amaretto`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser3(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users3)

    useEffect(() => {
        fetch(`/restaurant/getRestaurant/Seasonal Tastes`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser4(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users4)

    useEffect(() => {
        fetch(`/restaurant/getRestaurant/POTLI RESTAURANT`).then((result) => {
            result.json().then((resp) => {
                // console.warn(resp)
                setUser5(resp)
                console.log(resp.data);
            })
        })
    }, [])
    console.warn(users5)


    const PostData = async (e) => {

        e.preventDefault();
        const {
            favouriteRestaurant

        } = userFav;

        const res = await fetch('/favourite/player/8/restaurant/4/add', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({

                favouriteRestaurant,

            })
        })

        const data = await res.json();
        if (data.status === !data) {
            window.alert(" invalid");
            console.log(" invalid ")
        }
        else {
            window.alert(" Successfull");
            console.log(" Successfull ");
        }

    }


    const handleChange = (e) => {
        let isChecked = e.target.checked;
        alert("Checkbox value" + isChecked)
        setUserFav({ favouriteRestaurant: isChecked });
        // do whatever you want with isChecked value
    }


    return (
        <> 
          <HomePage/>
      
        <div>
            <h2 className="MoreAbout"> More About Restaurant Player 3</h2>

            <div className='container'>

                <div className='AllRowStyle'>
                    <Table striped bordered hover >
                        <thead >
                            <tr className='TableStyle' >

                                <th>Name</th>
                                <th>View All Details</th>

                                <th> Make Favorite Restaurant </th>



                            </tr>
                        </thead>
                        <tbody>
                            <tr >


                                <td>{users.name}</td>

                                <td> <Link to='/Restaurant1Data1' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View More</td>
                                </Link>
                                </td>
                                <td>

                                    <div className='MakeFavRest'>

                                        <Form method="POST">

                                            <input style={{ height: "25px", width: '25px', marginTop: "5px" }} type="checkbox" name="favouriteRestaurant" value={userFav.favouriteRestaurant} onChange={handleChange} />

                                        </Form>

                                        <br />
                                        <Button style={{ padding: '0px 20px', fontSize: '18px' }} className="center d-Flex justify-content-center" type="submit" name="signup" value='register' onClick={PostData} variant="primary">
                                            Submit
                                        </Button>


                                    </div>

                                </td>
                            </tr>

                        </tbody>
                        <tbody>
                            <tr >


                                <td>{users2.name}</td>

                                <td> <Link to='/Restaurant1Data2' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View More</td>
                                </Link>
                                </td>
                                <td>

                                    <div className='MakeFavRest'>

                                        <Form method="POST">

                                            <input style={{ height: "25px", width: '25px', marginTop: "5px" }} type="checkbox" name="favouriteRestaurant" value={userFav.favouriteRestaurant} onChange={handleChange} />

                                        </Form>

                                        <br />
                                        <Button style={{ padding: '0px 20px', fontSize: '18px' }} className="center d-Flex justify-content-center" type="submit" name="signup" value='register' onClick={PostData} variant="primary">
                                            Submit
                                        </Button>


                                    </div>

                                </td>
                            </tr>

                        </tbody>
                        <tbody>
                            <tr >


                                <td>{users3.name}</td>

                                <td> <Link to='/Restaurant1Data3' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View More</td>
                                </Link>
                                </td>
                                <td>

                                    <div className='MakeFavRest'>

                                        <Form method="POST">

                                            <input style={{ height: "25px", width: '25px', marginTop: "5px" }} type="checkbox" name="favouriteRestaurant" value={userFav.favouriteRestaurant} onChange={handleChange} />

                                        </Form>

                                        <br />
                                        <Button style={{ padding: '0px 20px', fontSize: '18px' }} className="center d-Flex justify-content-center" type="submit" name="signup" value='register' onClick={PostData} variant="primary">
                                            Submit
                                        </Button>


                                    </div>

                                </td>
                            </tr>

                        </tbody>
                        <tbody>
                            <tr >


                                <td>{users4.name}</td>

                                <td> <Link to='/Restaurant1Data4' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View More</td>
                                </Link>
                                </td>
                                <td>

                                    <div className='MakeFavRest'>

                                        <Form method="POST">

                                            <input style={{ height: "25px", width: '25px', marginTop: "5px" }} type="checkbox" name="favouriteRestaurant" value={userFav.favouriteRestaurant} onChange={handleChange} />

                                        </Form>

                                        <br />
                                        <Button style={{ padding: '0px 20px', fontSize: '18px' }} className="center d-Flex justify-content-center" type="submit" name="signup" value='register' onClick={PostData} variant="primary">
                                            Submit
                                        </Button>


                                    </div>

                                </td>
                            </tr>

                        </tbody>
                        <tbody>
                            <tr >


                                <td>{users5.name}</td>

                                <td> <Link to='/Restaurant1Data5' style={{ textDecoration: 'none' }}>
                                    <td style={{ Margin: "5px" }}> View More</td>
                                </Link>
                                </td>
                                <td>

                                    <div className='MakeFavRest'>

                                        <Form method="POST">

                                            <input style={{ height: "25px", width: '25px', marginTop: "5px" }} type="checkbox" name="favouriteRestaurant" value={userFav.favouriteRestaurant} onChange={handleChange} />

                                        </Form>

                                        <br />
                                        <Button style={{ padding: '0px 20px', fontSize: '18px' }} className="center d-Flex justify-content-center" type="submit" name="signup" value='register' onClick={PostData} variant="primary">
                                            Submit
                                        </Button>


                                    </div>

                                </td>
                            </tr>

                        </tbody>
                    </Table>




                </div>
            </div>

        </div>
        </>
    );
}
export default MoreAboutRestaurantPlayer8; 