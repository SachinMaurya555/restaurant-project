import React, { useState } from "react";
import Col from 'react-bootstrap/Col';
import HomePage from "./HomePage";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import './PlayerRagestationPage.css'
// import axios from 'axios';
import Row from 'react-bootstrap/Row';

const RestaurantRag = () => {

    const [user, setUser] = useState({
        name: '',
        contact_number: '',
        r_address_line1: '',
        r_address_line2: '',
        r_street_no: '',
        r_country: '',
        r_state: '',
        r_city: '',
        r_postal: '',
        mondayOpeningTime: '',
        mondayClosingTime: '',
        tuesdayOpeningTime: '',
        tuesdayClosingTime: '',
        wednesdayOpeningTime: '',
        wednesdayClosingTime: '',
        thursdayOpeningTime: '',
        thursdayClosingTime: '',
        fridayOpeningTime: '',
        fridayClosingTime: '',
        saturdayOpeningTime: '',
        saturdayClosingTime: '',
        sundayOpeningTime: '',
        sundayClosingTime: '',

    })


    let name, value;
    const handleInputs = (e) => {
        console.log(e);
        name = e.target.name;
        value = e.target.value;

        setUser({ ...user, [name]: value });
    }

    const PostData = async (e) => {

        e.preventDefault();
        const
            { name,
                contact_number,
                r_address_line1,
                r_address_line2,
                r_street_no,
                r_country,
                r_state,
                r_city,
                r_postal,
                mondayOpeningTime,
                mondayClosingTime,
                tuesdayOpeningTime,
                tuesdayClosingTime,
                wednesdayOpeningTime,
                wednesdayClosingTime,
                thursdayOpeningTime,
                thursdayClosingTime,
                fridayOpeningTime,
                fridayClosingTime,
                saturdayOpeningTime,
                saturdayClosingTime,
                sundayOpeningTime,
                sundayClosingTime
            } = user;

        const res = await fetch('/restaurant/addRestaurant', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(
                {
                    name,
                    contact_number,
                    r_address_line1,
                    r_address_line2,
                    r_street_no,
                    r_country,
                    r_state,
                    r_city,
                    r_postal,
                    mondayOpeningTime,
                    mondayClosingTime,
                    tuesdayOpeningTime,
                    tuesdayClosingTime,
                    wednesdayOpeningTime,
                    wednesdayClosingTime,
                    thursdayOpeningTime,
                    thursdayClosingTime,
                    fridayOpeningTime,
                    fridayClosingTime,
                    saturdayOpeningTime,
                    saturdayClosingTime,
                    sundayOpeningTime,
                    sundayClosingTime
                }

            )
        })

        const data = await res.json();
        if (data.status === !data) {
            window.alert(" invalid");
            console.log(" invalid ")
        }
        else {
            window.alert(" Successfull");
            console.log(" Successfull ");
        }

    }






    return (
        <>
            <HomePage />
            <div className="container">
                <h1 style={{ textAlign: "center", color: 'rgb(0 12 255)' }} > Restaurant Registration Form </h1>
                <Form method="POST" id="RestBackCSS">

                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" name="name" value={user.name} onChange={handleInputs} placeholder="Enter Your Name" />
                    </Form.Group>



                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                        <Form.Label>Contact
                            Number</Form.Label>
                        <Form.Control type="Number" name="contact_number" value={user.contact_number} onChange={handleInputs} placeholder=" Your Contact
                        Number " />
                    </Form.Group>


                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                        <Form.Label>Street
                            Address 1</Form.Label>
                        <Form.Control type="text" name="r_address_line1" value={user.r_address_line1} onChange={handleInputs} placeholder=" Your Street 
Address" />

                    </Form.Group>

                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                        <Form.Label>Street
                            Address 2</Form.Label>
                        <Form.Control type="text" name="r_address_line2" value={user.r_address_line2} onChange={handleInputs} placeholder=" Your Street 
Address" />

                        <Row>
                            <Col >
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Street NO
                                    </Form.Label>
                                    <Form.Control type="text" name="r_street_no" value={user.r_street_no} onChange={handleInputs} placeholder=" Your Street NO 
     
" />
                                </Form.Group>

                            </Col>
                            <Col >
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Country
                                    </Form.Label>
                                    <Form.Control type="text" name="r_country" value={user.r_country} onChange={handleInputs} placeholder=" Your Country 
     
" />
                                </Form.Group>

                            </Col>
                            <Col >
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>State
                                    </Form.Label>
                                    <Form.Control type="text" name="r_state" value={user.r_state} onChange={handleInputs} placeholder=" Your State 
     
" />
                                </Form.Group>

                            </Col>
                        </Row>
                        <Row>
                            <Col >
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>City
                                    </Form.Label>
                                    <Form.Control type="text" name="r_city" value={user.r_city} onChange={handleInputs} placeholder=" Your City 
     
" />
                                </Form.Group>

                            </Col>
                            <Col >
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Postal
                                    </Form.Label>
                                    <Form.Control type="text" name="r_postal" value={user.r_postal} onChange={handleInputs} placeholder=" Your Postal code 
     
" />
                                </Form.Group>

                            </Col>
                        </Row>
                    </Form.Group>


                    <Row>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Set MondayOpeningTime :- </Form.Label>

                                <Form.Control type="time" name="mondayOpeningTime" value={user.mondayOpeningTime} onChange={handleInputs} placeholder=" MondayOpeningTime
     
     " />
                            </Form.Group>
                        </Col>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Set mondayClosingTime :- </Form.Label>

                                <Form.Control type="time" name="mondayClosingTime" value={user.mondayClosingTime} onChange={handleInputs} placeholder=" mondayClosingTime
     
     " />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Set tuesdayOpeningTime :- </Form.Label>

                                <Form.Control type="time" name="tuesdayOpeningTime" value={user.tuesdayOpeningTime} onChange={handleInputs} placeholder=" tuesdayOpeningTime
     
     " />
                            </Form.Group>
                        </Col>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Set tuesdayClosingTime :- </Form.Label>

                                <Form.Control type="time" name="tuesdayClosingTime" value={user.tuesdayClosingTime} onChange={handleInputs} placeholder=" tuesdayClosingTime
     
     " />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Set wednesdayOpeningTime :- </Form.Label>

                                <Form.Control type="time" name="wednesdayOpeningTime" value={user.wednesdayOpeningTime} onChange={handleInputs} placeholder=" wednesdayOpeningTime
     
     " />
                            </Form.Group>
                        </Col>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Set wednesdayClosingTime :- </Form.Label>

                                <Form.Control type="time" name="wednesdayClosingTime" value={user.wednesdayClosingTime} onChange={handleInputs} placeholder=" wednesdayClosingTime
     
     " />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Set thursdayOpeningTime :- </Form.Label>

                                <Form.Control type="time" name="thursdayOpeningTime" value={user.thursdayOpeningTime} onChange={handleInputs} placeholder=" thursdayOpeningTime
     
     " />
                            </Form.Group>
                        </Col>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Set thursdayClosingTime :- </Form.Label>

                                <Form.Control type="time" name="thursdayClosingTime" value={user.thursdayClosingTime} onChange={handleInputs} placeholder=" thursdayClosingTime
     
     " />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Set fridayOpeningTime :- </Form.Label>

                                <Form.Control type="time" name="fridayOpeningTime" value={user.fridayOpeningTime} onChange={handleInputs} placeholder=" fridayOpeningTime
     
     " />
                            </Form.Group>
                        </Col>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Set fridayClosingTime :- </Form.Label>

                                <Form.Control type="time" name="fridayClosingTime" value={user.fridayClosingTime} onChange={handleInputs} placeholder=" fridayClosingTime
     
     " />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Set saturdayOpeningTime :- </Form.Label>

                                <Form.Control type="time" name="saturdayOpeningTime" value={user.saturdayOpeningTime} onChange={handleInputs} placeholder=" saturdayOpeningTime
     
     " />
                            </Form.Group>
                        </Col>
                        <Col >

                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Set saturdayClosingTime :- </Form.Label>

                                <Form.Control type="time" name="saturdayClosingTime" value={user.saturdayClosingTime} onChange={handleInputs} placeholder=" saturdayClosingTime
     
     " />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col >

                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Set sundayOpeningTime :- </Form.Label>

                                <Form.Control type="time" name="sundayOpeningTime" value={user.sundayOpeningTime} onChange={handleInputs} placeholder=" sundayOpeningTime
     
     " />
                            </Form.Group>
                        </Col>
                        <Col >

                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Set sundayClosingTime :- </Form.Label>

                                <Form.Control type="time" name="sundayClosingTime" value={user.sundayClosingTime} onChange={handleInputs} placeholder=" sundayClosingTime
     
     " />
                            </Form.Group>
                        </Col>
                    </Row>



                </Form>

                <br />

                <center>
                    <Button type="submit" name="signup" value='register' onClick={PostData} variant="primary" size="lg">
                        Submit Data
                    </Button>
                </center>


            </div>
        </>
    )
}

export default RestaurantRag;