import React, { useState } from "react";
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import './PlayerRagestationPage.css'
import HomePage from "./HomePage";

const PlayerRag = () => {

    const [check, setCheck] = useState(false)
    const [check2, setCheck2] = useState(false)
    const [user, setUser] = useState({

        firstName: '',
        lastName: '',
        dob: '',
        age: '',
        p_street_no: '',
        p_address_line1: '',
        p_address_line2: '',
        p_city: '',
        p_postal: '',
        p_state: '',
        p_country: '',
        a_street_no: '',
        a_address_line1: '',
        a_address_line2: '',
        a_city: '',
        a_postal: '',
        a_state: '',
        a_country: '',
        o_unit_no: '',
        o_address_line1: '',
        o_address_line2: '',
        o_city: '',
        o_postal: '',
        o_state: '',
        o_country: '',
        phone_number: '',
        email: '',
        driving_license: '',
        passport: ''

    })

    let name, value;
    const handleInputs = (e) => {
        console.log(e);
        name = e.target.name;
        value = e.target.value;

        setUser({ ...user, [name]: value });
    }



    const checkboxHandler = (checked) => {

        user.a_address_line1 = user.p_address_line1;
        user.a_address_line2 = user.p_address_line2;
        user.a_city = user.p_city;
        user.a_postal = user.p_postal;
        user.a_street_no = user.p_street_no;
        user.a_state = user.p_state;
        user.a_country = user.p_country;

        setUser({ ...user })
    }

    const checkboxHandler2 = (checked) => {

        user.o_address_line1 = user.p_address_line1;
        user.o_address_line2 = user.p_address_line2;
        user.o_city = user.p_city;
        user.o_postal = user.p_postal;
        user.o_unit_no = user.p_street_no;
        user.o_state = user.p_state;
        user.o_country = user.p_country;


        setUser({ ...user })
    }


    const PostData = async (e) => {

        e.preventDefault();
        const {

            firstName,
            lastName,
            dob,
            age,
            p_street_no,
            p_address_line1,
            p_address_line2,
            p_city,
            p_postal,
            p_state,
            p_country,
            a_street_no,
            a_address_line1,
            a_address_line2,
            a_city,
            a_postal,
            a_state,
            a_country,
            o_unit_no,
            o_address_line1,
            o_address_line2,
            o_city,
            o_postal,
            o_state,
            o_country,
            phone_number,
            email,
            driving_license,
            passport,
        } = user;

        const res = await fetch("/player/addPlayer", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({


                firstName,
                lastName,
                dob,
                age,
                p_street_no,
                p_address_line1,
                p_address_line2,
                p_city,
                p_postal,
                p_state,
                p_country,
                a_street_no,
                a_address_line1,
                a_address_line2,
                a_city,
                a_postal,
                a_state,
                a_country,
                o_unit_no,
                o_address_line1,
                o_address_line2,
                o_city,
                o_postal,
                o_state,
                o_country,
                phone_number,
                email,
                driving_license,
                passport,
            })
        })

        const data = await res.json();
        if (data.status === !data) {
            window.alert(" invalid");
            console.log(" invalid ")
        }
        else {
            window.alert(" Successfull");
            console.log(" Successfull ");
        }

    }



    return (
        <>
            <HomePage />
            <div className="container">
                <h1 style={{ textAlign: "center", color: 'rgb(0 12 255)' }}> Player Registration Form </h1>
                <Form >


                    <Row>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>FirstName</Form.Label>
                                <Form.Control type="text" name="firstName" value={user.firstName} onChange={handleInputs} placeholder="Enter Your FirstName" />
                            </Form.Group>
                        </Col>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>LastName</Form.Label>
                                <Form.Control type="text" name="lastName" value={user.lastName} onChange={handleInputs} placeholder="Enter Your LastName" />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col >

                            <Form.Group className="mb-2" controlId="dob">
                                <Form.Label>Date Of Birth</Form.Label>
                                <Form.Control type="date" name="dob" value={user.dob} onChange={handleInputs} placeholder="Date of Birth" />
                            </Form.Group>
                        </Col>
                        <Col >
                            <Form.Group className="mb-2" controlId="dob">
                                <Form.Label>Age</Form.Label>
                                <Form.Control type="text" name="age" value={user.age} onChange={handleInputs} placeholder=" Enter your age" />
                            </Form.Group>

                        </Col>
                    </Row>

                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                        <Form.Label>Primary
                            Address 1</Form.Label>
                        <Form.Control type="text" name="p_address_line1" value={user.p_address_line1} onChange={handleInputs} placeholder=" Your Primary 
Address" />

                    </Form.Group>

                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                        <Form.Label>Primary
                            Address 2</Form.Label>
                        <Form.Control type="text" name="p_address_line2" value={user.p_address_line2} onChange={handleInputs} placeholder=" Your Primary 
Address" />

                        <Row>
                            <Col >
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Street NO
                                    </Form.Label>
                                    <Form.Control type="text" name="p_street_no" value={user.p_street_no} onChange={handleInputs} placeholder=" Your Street NO 
     
" />
                                </Form.Group>

                            </Col>
                            <Col >
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Country
                                    </Form.Label>
                                    <Form.Control type="text" name="p_country" value={user.p_country} onChange={handleInputs} placeholder=" Your Country 
     
" />
                                </Form.Group>

                            </Col>
                            <Col >
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>State
                                    </Form.Label>
                                    <Form.Control type="text" name="p_state" value={user.p_state} onChange={handleInputs} placeholder=" Your State 
     
" />
                                </Form.Group>

                            </Col>
                        </Row>
                        <Row>
                            <Col >
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>City
                                    </Form.Label>
                                    <Form.Control type="text" name="p_city" value={user.p_city} onChange={handleInputs} placeholder=" Your City 
     
" />
                                </Form.Group>

                            </Col>
                            <Col >
                                <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Postal
                                    </Form.Label>
                                    <Form.Control type="text" name="p_postal" value={user.p_postal} onChange={handleInputs} placeholder=" Your Zip code 
     
" />
                                </Form.Group>

                            </Col>
                        </Row>
                    </Form.Group>

                    <h3>Alternate Adreess Same As : Primary Adreess </h3>

                    <div className="check" htmlFor="checkbox">
                        <input type="checkbox" style={{ height: "25px", width: '25px', marginTop: "5px" }} name="noway" value='false' onChange={() => checkboxHandler(check)} />


                        <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                            <Form.Label>Alternate
                                Address-1</Form.Label>
                            <Form.Control type="text" name="a_address_line1" value={check ? user.p_address_line1 : user.a_address_line1} onChange={handleInputs} placeholder=" Your Alternate 
Address" />
                        </Form.Group>
                        <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                            <Form.Label>Alternate
                                Address-2</Form.Label>
                            <Form.Control type="text" name="a_address_line2" value={check ? user.p_address_line2 : user.a_address_line2} onChange={handleInputs} placeholder=" Your Alternate 
Address" />
                            <Row>
                                <Col >
                                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Unit No
                                        </Form.Label>
                                        <Form.Control type="text" name="a_street_no" value={check ? user.p_street_no : user.a_street_no} onChange={handleInputs} placeholder=" Your Unit No  

" />
                                    </Form.Group>

                                </Col>
                                <Col >
                                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Country
                                        </Form.Label>
                                        <Form.Control type="text" name="a_country" value={check ? user.p_country : user.a_country} onChange={handleInputs} placeholder=" Your Country 

" />
                                    </Form.Group>

                                </Col>
                                <Col >
                                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                        <Form.Label>State
                                        </Form.Label>
                                        <Form.Control type="text" name="a_state" value={check ? user.p_state : user.a_state} onChange={handleInputs} placeholder=" Your State 

" />
                                    </Form.Group>

                                </Col>
                            </Row>
                            <Row>
                                <Col >
                                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                        <Form.Label>City
                                        </Form.Label>
                                        <Form.Control type="text" name="a_city" value={check ? user.p_city : user.a_city} onChange={handleInputs} placeholder=" Your City 

" />
                                    </Form.Group>

                                </Col>
                                <Col >
                                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Postal
                                        </Form.Label>
                                        <Form.Control type="text" name="a_postal" value={check ? user.p_postal : user.a_postal} onChange={handleInputs} placeholder=" Your Zip code 

" />
                                    </Form.Group>

                                </Col>
                            </Row>
                        </Form.Group>

                    </div>


                    <h3> Office Adreess Same As : Primary Adreess </h3>


                    <div className="check2" htmlFor="checkbox">
                        <input type="checkbox" style={{ height: "25px", width: '25px', marginTop: "5px" }} name="noway" value='false' onChange={() => checkboxHandler2(check2)} />

                        <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                            <Form.Label>Office
                                Address-1</Form.Label>
                            <Form.Control type="text" name="o_address_line1" value={check2 ? user.p_address_line1 : user.o_address_line1} onChange={handleInputs} placeholder=" Your Alternate 
Address" />
                        </Form.Group>
                        <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                            <Form.Label>Office
                                Address-2</Form.Label>
                            <Form.Control type="text" name="o_address_line2" value={check2 ? user.p_address_line2 : user.o_address_line2} onChange={handleInputs} placeholder=" Your Alternate 
Address" />
                            <Row>
                                <Col >
                                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Unit No
                                        </Form.Label>
                                        <Form.Control type="text" name="o_unit_no" value={check2 ? user.p_street_no : user.o_unit_no} onChange={handleInputs} placeholder=" Your Unit No  
     
" />
                                    </Form.Group>

                                </Col>
                                <Col >
                                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Country
                                        </Form.Label>
                                        <Form.Control type="text" name="o_country" value={check2 ? user.p_country : user.o_country} onChange={handleInputs} placeholder=" Your Country 
     
" />
                                    </Form.Group>

                                </Col>
                                <Col >
                                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                        <Form.Label>State
                                        </Form.Label>
                                        <Form.Control type="text" name="o_state" value={check2 ? user.p_state : user.o_state} onChange={handleInputs} placeholder=" Your State 
     
" />
                                    </Form.Group>

                                </Col>
                            </Row>
                            <Row>
                                <Col >
                                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                        <Form.Label>City
                                        </Form.Label>
                                        <Form.Control type="text" name="o_city" value={check2 ? user.p_city : user.o_city} onChange={handleInputs} placeholder=" Your City 
     
" />
                                    </Form.Group>

                                </Col>
                                <Col >
                                    <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Postal
                                        </Form.Label>
                                        <Form.Control type="text" name="o_postal" value={check2 ? user.p_postal : user.o_postal} onChange={handleInputs} placeholder=" Your Zip code 
     
" />
                                    </Form.Group>

                                </Col>
                            </Row>
                        </Form.Group>

                    </div>

                    <Row>
                        <Col >

                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Mobile
                                    Number</Form.Label>
                                <Form.Control type="Number" name="phone_number" value={user.phone_number} onChange={handleInputs} placeholder=" Your Mobile 
Number" />
                            </Form.Group>
                        </Col>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" name="email" value={user.email} onChange={handleInputs} placeholder=" Your Email 
" />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col >

                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Driver’s
                                    License</Form.Label>
                                <Form.Control type="text" name="driving_license" value={user.driving_license} onChange={handleInputs} placeholder=" Your Driver’s 
    License 
" />
                            </Form.Group>
                        </Col>
                        <Col >
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                                <Form.Label>Passport
                                </Form.Label>
                                <Form.Control type="text" name="passport" value={user.passport} onChange={handleInputs} placeholder=" Your Passport 
     
" />
                            </Form.Group>
                        </Col>
                    </Row>
                </Form>

                <br />

                <center>
                    <Button type="submit" name="signup" value='register' onClick={PostData} variant="primary" size="lg">
                        Submit Data
                    </Button>
                </center>


            </div>
        </>
    )
}

export default PlayerRag;